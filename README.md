**Self Chaining Blocks** can create  stronger and faster blockchain  **without Mining or Staking**. The secret lies  in consumer wallet.

This project explores a possibility that can be used as an alternative for proof of work in block chain technology. While investigating Beal Conjecture, I realized that some of its properties can be suitable for deciding next block signers and validators in a decentralized consensus manner. Currently, there is no alternative to proof of work without a minimal central authority. This particular proposal weaves an algorithm that can self govern the task of selecting both the next block signer and validators. The primary advantage of this algorithm is that a single block can be created by nodes in a couple seconds in comparison to the current 10 minute requirement. Here, the blocks are not mined, but are signed by an independent node using a private key and, consequently, are validated by multiple validator nodes.

[Refer this PDF for details](SelfChainingBlocks.pdf)
[Sample blockchain is hosted at] (https://self-chaining-blocks.gitlab.io/self-chaining-blocks/)

### Why another chain technology?
Even though bitcoin and other blockchains became very popular there are only few  decentralized algorithms for consensus. Mainly, proof of work or proof of stake.   Proof of work is not efficient and proof of stake is not a technical solution. Self chaining blocks provides a powerful alternate based on strong mathematical structures. 

### Crypto Enthusiasts


Also, there are opportunities  to collaborate in design, development and operationalization of innovative decentralized blockchain( Self Chaining Blocks) based on my white-paper [Self Chaining Blocks](/uploads/1a7acbb3a82a08078bb0b9dcf3cdd15c/SelfChainingBlocks.pdf). Of course your contribution will be compensated with our native crypto currency!  To learn more about this effort and to contribute,  please request your access to this  gitlab project [code and documentation](https://gitlab.com/self-chaining-blocks). Also, there are demo-able use cases available if anyone is interested in this.    Note: currently this project is not open sourced yet and please treat this as private one. 

Personally, this project provided me a diversion during covid-19 lockdown. You can reach me via selfchainingblocks ( at) gmail.com.  Or  [https://twitter.com/RajuMariappan](https://twitter.com/RajuMariappan)

Planning to upload some videos in 2022. Subscribe to https://www.youtube.com/channel/UC0gJpIz_S2cfZ8lNB05s8DA

Slide deck for Presentation 1: https://docs.google.com/presentation/d/1QhOSgEcR90xmOsxYnEfg61vv_HdT1IFomFbVhHeXAcc/edit?usp=sharing




